export default {
  ssr: false, // Disable Server Side rendering
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map';
      }
    }
  },
  target: 'static',

  generate: {
    dir: 'public', // Output directory for the generated site
  },

  dir: {
    public: 'static', // Use 'static' folder for static files
  },

  css: [
    '~/static/css/main.css'
  ],

  site: {
    url: process.env.BASE_URL,
    name: 'Nexa',
    description: '',
    defaultLocale: 'en'
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },

  image: {
    quality: 80,
    screens: {
      'xs': 320,
      'sm': 640,
      'md': 768,
      'lg': 1024,
      'xl': 1280,
      'xxl': 1536,
      '2xl': 1536
    },
    format: ['webp', 'avif', 'jpeg', 'png']
  },

  nitro: {
    prerender: {
      crawlLinks: true,
      routes: ['/'],
      failOnError: false,
    },
  },

  plugins: [
    '~/plugins/font-awesome.js',
    '~/plugins/pinia.js',
    '~/plugins/directives.js'
  ],

  components: [
    {
      path: '@/components',
      pathPrefix: false
    }
  ],

  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL,
      siteEnv: process.env.NUXT_SITE_ENV || 'development'
    }
  },

  modules: ["@nuxtjs/seo", "@nuxt/image", "nuxt-security", "@nuxt/ui", 'nuxt-security'],
  // Following configuration is only necessary to make Stackblitz work correctly.
  // For local projects, you do not need any configuration to try it out.
  security: {
    headers: {
      crossOriginResourcePolicy: 'cross-origin',
      contentSecurityPolicy: false,
    },
  },

  compatibilityDate: '2024-07-06'
};